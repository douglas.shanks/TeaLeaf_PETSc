#!/bin/bash

echo "Launching TeaLeaf2d petsc with ARM compiler"

compiler="arm"

for i in 1 2 4 8 16 32;
do
	sbatch --nodes $i --output ./profile/${compiler}/tl_cggamg_${compiler}_n_$i.txt tea_job_${compiler}.pbs
done

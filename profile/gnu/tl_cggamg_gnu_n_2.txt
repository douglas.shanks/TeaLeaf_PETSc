
   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1


   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1

 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Conduction error  0.6436177E-18
Iteration count       17
      Solve Time    5.2873799801    Its     17     Time Per It    0.3110223518
 Wall clock    5.2893691062927246     
 Average time per cell    3.3058556914329528E-007
 Step time per cell       3.3057206869125365E-007
 Step       2 time   0.0040000 timestep   4.00E-03
Conduction error  0.1109670E-14
Iteration count       17
      Solve Time    3.2020969391    Its     17     Time Per It    0.1883586435
 Wall clock    8.4923081398010254     
 Average time per cell    2.6538462936878202E-007
 Step time per cell       2.0015794038772584E-007
 Step       3 time   0.0080000 timestep   4.00E-03
Conduction error  0.2745322E-14
Iteration count       17
      Solve Time    2.8964920044    Its     17     Time Per It    0.1703818826
 Wall clock    11.389648199081421     
 Average time per cell    2.3728433748086293E-007
 Step time per cell       1.8105626106262207E-007
 Step       4 time   0.0120000 timestep   4.00E-03
Conduction error  0.3601992E-14
Iteration count       17
      Solve Time    2.8870170116    Its     17     Time Per It    0.1698245301
 Wall clock    14.277522087097168     
 Average time per cell    2.2308628261089324E-007
 Step time per cell       1.8046605587005616E-007
 Step       5 time   0.0160000 timestep   4.00E-03
Conduction error  0.4140935E-14
Iteration count       17
      Solve Time    2.9591329098    Its     17     Time Per It    0.1740666418
 Wall clock    17.237541198730469     
 Average time per cell    2.1546926498413085E-007
 Step time per cell       1.8497182428836823E-007
 Step       6 time   0.0200000 timestep   4.00E-03
Conduction error  0.4538709E-14
Iteration count       17
      Solve Time    2.8717730045    Its     17     Time Per It    0.1689278238
 Wall clock    20.110156059265137     
 Average time per cell    2.0948079228401185E-007
 Step time per cell       1.7951093614101410E-007
 Step       7 time   0.0240000 timestep   4.00E-03
Conduction error  0.4868297E-14
Iteration count       17
      Solve Time    2.9325540066    Its     17     Time Per It    0.1725031769
 Wall clock    23.043569087982178     
 Average time per cell    2.0574615257126945E-007
 Step time per cell       1.8331225216388701E-007
 Step       8 time   0.0280000 timestep   4.00E-03
Conduction error  0.5159965E-14
Iteration count       17
      Solve Time    2.9212329388    Its     17     Time Per It    0.1718372317
 Wall clock    25.965651035308838     
 Average time per cell    2.0285664871335028E-007
 Step time per cell       1.8260300159454345E-007
 Step       9 time   0.0320000 timestep   4.00E-03
Conduction error  0.5424125E-14
Iteration count       17
      Solve Time    2.8764240742    Its     17     Time Per It    0.1692014161
 Wall clock    28.842921018600464     
 Average time per cell    2.0029806262916988E-007
 Step time per cell       1.7980337142944336E-007
 Step      10 time   0.0360000 timestep   4.00E-03
Conduction error  0.5664729E-14
Iteration count       17
      Solve Time    2.9115879536    Its     17     Time Per It    0.1712698796
 Wall clock    31.756376028060913     
 Average time per cell    1.9847735017538070E-007
 Step time per cell       1.8206255137920381E-007
Test problem   5 is within   0.1214445E-08% of the expected solution
 This test is considered PASSED
 First step overhead   2.0866250991821289     
 Wall clock    31.758141040802002     

Profiler Output                 Time            Percentage
Timestep              :          0.0068          0.0216
Halo Exchange         :          0.0215          0.0677
Summary               :          0.0025          0.0080
Visit                 :          0.0000          0.0000
Tea Init              :          0.0743          0.2340
Dot Product           :          0.0065          0.0205
Tea Solve             :         31.6546         99.6739
Tea Reset             :          0.0089          0.0279
Set Field             :          0.0002          0.0007
Total                 :         31.7753        100.0542
The Rest              :         -0.0172         -0.0542

   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1


   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1

 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Conduction error  0.6436177E-18
Iteration count       17
      Solve Time    2.9976577759    Its     17     Time Per It    0.1763328103
 Wall clock    2.9996562004089355     
 Average time per cell    1.8747851252555846E-007
 Step time per cell       1.8746325373649598E-007
 Step       2 time   0.0040000 timestep   4.00E-03
Conduction error  0.1109670E-14
Iteration count       17
      Solve Time    2.9073190689    Its     17     Time Per It    0.1710187688
 Wall clock    5.9078500270843506     
 Average time per cell    1.8462031334638596E-007
 Step time per cell       1.8173374235630035E-007
 Step       3 time   0.0080000 timestep   4.00E-03
Conduction error  0.2745322E-14
Iteration count       17
      Solve Time    2.8800599575    Its     17     Time Per It    0.1694152916
 Wall clock    8.7887861728668213     
 Average time per cell    1.8309971193472545E-007
 Step time per cell       1.8003188073635101E-007
 Step       4 time   0.0120000 timestep   4.00E-03
Conduction error  0.3601992E-14
Iteration count       17
      Solve Time    2.9381098747    Its     17     Time Per It    0.1728299926
 Wall clock    11.727794170379639     
 Average time per cell    1.8324678391218186E-007
 Step time per cell       1.8365962803363800E-007
 Step       5 time   0.0160000 timestep   4.00E-03
Conduction error  0.4140935E-14
Iteration count       17
      Solve Time    2.8736679554    Its     17     Time Per It    0.1690392915
 Wall clock    14.602343082427979     
 Average time per cell    1.8252928853034974E-007
 Step time per cell       1.7963255941867829E-007
 Step       6 time   0.0200000 timestep   4.00E-03
Conduction error  0.4538709E-14
Iteration count       17
      Solve Time    2.9341509342    Its     17     Time Per It    0.1725971138
 Wall clock    17.537376165390015     
 Average time per cell    1.8268100172281265E-007
 Step time per cell       1.8341106176376342E-007
 Step       7 time   0.0240000 timestep   4.00E-03
Conduction error  0.4868297E-14
Iteration count       17
      Solve Time    2.9394149780    Its     17     Time Per It    0.1729067634
 Wall clock    20.477679014205933     
 Average time per cell    1.8283641976969582E-007
 Step time per cell       1.8374074995517732E-007
 Step       8 time   0.0280000 timestep   4.00E-03
Conduction error  0.5159965E-14
Iteration count       17
      Solve Time    2.8559138775    Its     17     Time Per It    0.1679949340
 Wall clock    23.334474086761475     
 Average time per cell    1.8230057880282401E-007
 Step time per cell       1.7852269113063811E-007
 Step       9 time   0.0320000 timestep   4.00E-03
Conduction error  0.5424125E-14
Iteration count       17
      Solve Time    2.9221141338    Its     17     Time Per It    0.1718890667
 Wall clock    26.257475137710571     
 Average time per cell    1.8234357734521231E-007
 Step time per cell       1.8265874683856963E-007
 Step      10 time   0.0360000 timestep   4.00E-03
Conduction error  0.5664729E-14
Iteration count       17
      Solve Time    2.8725099564    Its     17     Time Per It    0.1689711739
 Wall clock    29.131805181503296     
 Average time per cell    1.8207378238439559E-007
 Step time per cell       1.7961587011814118E-007
Test problem   5 is within   0.1214488E-08% of the expected solution
 This test is considered PASSED
 First step overhead   9.1673135757446289E-002
 Wall clock    29.133454084396362     

Profiler Output                 Time            Percentage
Timestep              :          0.0068          0.0233
Halo Exchange         :          0.0268          0.0920
Summary               :          0.0024          0.0083
Visit                 :          0.0000          0.0000
Tea Init              :          0.0777          0.2666
Dot Product           :          0.0065          0.0223
Tea Solve             :         29.0263         99.6321
Tea Reset             :          0.0089          0.0306
Set Field             :          0.0004          0.0014
Total                 :         29.1558        100.0766
The Rest              :         -0.0223         -0.0766

   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1


   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    128
 Thread Count:     1

 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Conduction error  0.6436177E-18
Iteration count       17
      Solve Time    3.0034260750    Its     17     Time Per It    0.1766721221
 Wall clock    3.0055160522460938     
 Average time per cell    1.8784475326538085E-007
 Step time per cell       1.8783099949359893E-007
 Step       2 time   0.0040000 timestep   4.00E-03
Conduction error  0.1109670E-14
Iteration count       17
      Solve Time    2.8576419353    Its     17     Time Per It    0.1680965844
 Wall clock    5.8640360832214355     
 Average time per cell    1.8325112760066986E-007
 Step time per cell       1.7862901091575624E-007
 Step       3 time   0.0080000 timestep   4.00E-03
Conduction error  0.2745322E-14
Iteration count       17
      Solve Time    2.9472610950    Its     17     Time Per It    0.1733682997
 Wall clock    8.8121700286865234     
 Average time per cell    1.8358687559763591E-007
 Step time per cell       1.8423199653625488E-007
 Step       4 time   0.0120000 timestep   4.00E-03
Conduction error  0.3601992E-14
Iteration count       17
      Solve Time    2.8790960312    Its     17     Time Per It    0.1693585901
 Wall clock    11.692147970199585     
 Average time per cell    1.8268981203436850E-007
 Step time per cell       1.7996993660926820E-007
 Step       5 time   0.0160000 timestep   4.00E-03
Conduction error  0.4140935E-14
Iteration count       17
      Solve Time    2.9435479641    Its     17     Time Per It    0.1731498802
 Wall clock    14.636614084243774     
 Average time per cell    1.8295767605304719E-007
 Step time per cell       1.8400232493877410E-007
 Step       6 time   0.0200000 timestep   4.00E-03
Conduction error  0.4538709E-14
Iteration count       17
      Solve Time    2.9173288345    Its     17     Time Per It    0.1716075785
 Wall clock    17.554836034774780     
 Average time per cell    1.8286287536223730E-007
 Step time per cell       1.8236012756824493E-007
 Step       7 time   0.0240000 timestep   4.00E-03
Conduction error  0.4868297E-14
Iteration count       17
      Solve Time    2.8704581261    Its     17     Time Per It    0.1688504780
 Wall clock    20.426143884658813     
 Average time per cell    1.8237628468445370E-007
 Step time per cell       1.7942987382411957E-007
 Step       8 time   0.0280000 timestep   4.00E-03
Conduction error  0.5159965E-14
Iteration count       17
      Solve Time    2.9291341305    Its     17     Time Per It    0.1723020077
 Wall clock    23.356165885925293     
 Average time per cell    1.8247004598379136E-007
 Step time per cell       1.8309955298900605E-007
 Step       9 time   0.0320000 timestep   4.00E-03
Conduction error  0.5424125E-14
Iteration count       17
      Solve Time    2.8586537838    Its     17     Time Per It    0.1681561049
 Wall clock    26.215708971023560     
 Average time per cell    1.8205353452099694E-007
 Step time per cell       1.7869268357753754E-007
 Step      10 time   0.0360000 timestep   4.00E-03
Conduction error  0.5664729E-14
Iteration count       17
      Solve Time    2.9369568825    Its     17     Time Per It    0.1727621696
 Wall clock    29.154533863067627     
 Average time per cell    1.8221583664417267E-007
 Step time per cell       1.8364857137203215E-007
Test problem   5 is within   0.1214545E-08% of the expected solution
 This test is considered PASSED
 First step overhead  0.14723205566406250     
 Wall clock    29.155945062637329     

Profiler Output                 Time            Percentage
Timestep              :          0.0106          0.0365
Halo Exchange         :          0.0181          0.0621
Summary               :          0.0031          0.0105
Visit                 :          0.0000          0.0000
Tea Init              :          0.1042          0.3573
Dot Product           :          0.0291          0.0998
Tea Solve             :         29.0258         99.5536
Tea Reset             :          0.0089          0.0306
Set Field             :          0.0004          0.0014
Total                 :         29.2002        100.1518
The Rest              :         -0.0442         -0.1518

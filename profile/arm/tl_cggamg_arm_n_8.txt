 
   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    512
 Thread Count:     1
 
 
   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    512
 Thread Count:     1
 
 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Conduction error  0.6379851E-18
Iteration count       17
      Solve Time    1.1753568649    Its     17     Time Per It    0.0691386391
 Wall clock     1.177038908004761     
 Average time per cell    7.3564931750297548E-008
 Step time per cell       7.3562800884246821E-008
 Step       2 time   0.0040000 timestep   4.00E-03
Conduction error  0.1066305E-14
Iteration count       17
      Solve Time    0.7298190594    Its     17     Time Per It    0.0429305329
 Wall clock     1.908679962158203     
 Average time per cell    5.9646248817443853E-008
 Step time per cell       4.5635312795639036E-008
 Step       3 time   0.0080000 timestep   4.00E-03
Conduction error  0.2539188E-14
Iteration count       17
      Solve Time    0.7346739769    Its     17     Time Per It    0.0432161163
 Wall clock     2.645316123962402     
 Average time per cell    5.5110752582550049E-008
 Step time per cell       4.5939952135086063E-008
 Step       4 time   0.0120000 timestep   4.00E-03
Conduction error  0.3227957E-14
Iteration count       17
      Solve Time    0.7188718319    Its     17     Time Per It    0.0422865783
 Wall clock     3.366125106811523     
 Average time per cell    5.2595704793930051E-008
 Step time per cell       4.4950634241104129E-008
 Step       5 time   0.0160000 timestep   4.00E-03
Conduction error  0.3634224E-14
Iteration count       17
      Solve Time    0.8498179913    Its     17     Time Per It    0.0499892936
 Wall clock     4.217880964279175     
 Average time per cell    5.2723512053489687E-008
 Step time per cell       5.3134873509407045E-008
 Step       6 time   0.0200000 timestep   4.00E-03
Conduction error  0.3921679E-14
Iteration count       17
      Solve Time    0.7159872055    Its     17     Time Per It    0.0421168944
 Wall clock     4.935682058334351     
 Average time per cell    5.1413354774316154E-008
 Step time per cell       4.4770255684852603E-008
 Step       7 time   0.0240000 timestep   4.00E-03
Conduction error  0.4156725E-14
Iteration count       17
      Solve Time    0.7232189178    Its     17     Time Per It    0.0425422893
 Wall clock     5.660835981369019     
 Average time per cell    5.0543178405080525E-008
 Step time per cell       4.5222193002700802E-008
 Step       8 time   0.0280000 timestep   4.00E-03
Conduction error  0.4364243E-14
Iteration count       17
      Solve Time    0.7226040363    Its     17     Time Per It    0.0425061198
 Wall clock     6.385113000869751     
 Average time per cell    4.9883695319294930E-008
 Step time per cell       4.5167118310928346E-008
 Step       9 time   0.0320000 timestep   4.00E-03
Conduction error  0.4553778E-14
Iteration count       17
      Solve Time    0.7328531742    Its     17     Time Per It    0.0431090102
 Wall clock     7.118377923965454     
 Average time per cell    4.9433180027537876E-008
 Step time per cell       4.5825749635696413E-008
 Step      10 time   0.0360000 timestep   4.00E-03
Conduction error  0.2794031E-13
Iteration count       16
      Solve Time    0.6956908703    Its     16     Time Per It    0.0434806794
 Wall clock     7.816282033920288     
 Average time per cell    4.8851762712001803E-008
 Step time per cell       4.3519183993339542E-008
Test problem   5 is within   0.1059985E-08% of the expected solution
 This test is considered PASSED
 First step overhead   0.4468407630920410     
 Wall clock     7.818484067916870     
 
Profiler Output                 Time            Percentage
Timestep              :          0.0224          0.2863
Halo Exchange         :          0.0027          0.0340
Summary               :          0.0033          0.0428
Visit                 :          0.0000          0.0000
Tea Init              :          0.0165          0.2109
Dot Product           :          0.0016          0.0204
Tea Solve             :          7.7730         99.4178
Tea Reset             :          0.0015          0.0194
Set Field             :          0.0000          0.0004
Total                 :          7.8210        100.0319
The Rest              :         -0.0025         -0.0319
 
   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    512
 Thread Count:     1
 
 
   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    512
 Thread Count:     1
 
 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Conduction error  0.6379851E-18
Iteration count       17
      Solve Time    0.7993798256    Its     17     Time Per It    0.0470223427
 Wall clock    0.8009829521179199     
 Average time per cell    5.0061434507369996E-008
 Step time per cell       5.0059303641319276E-008
 Step       2 time   0.0040000 timestep   4.00E-03
Conduction error  0.1066305E-14
Iteration count       17
      Solve Time    0.7392470837    Its     17     Time Per It    0.0434851226
 Wall clock     1.542150020599365     
 Average time per cell    4.8192188143730161E-008
 Step time per cell       4.6224564313888551E-008
 Step       3 time   0.0080000 timestep   4.00E-03
Conduction error  0.2539188E-14
Iteration count       17
      Solve Time    0.7172539234    Its     17     Time Per It    0.0421914073
 Wall clock     2.261347055435181     
 Average time per cell    4.7111396988232927E-008
 Step time per cell       4.4851377606391906E-008
 Step       4 time   0.0120000 timestep   4.00E-03
Conduction error  0.3227957E-14
Iteration count       17
      Solve Time    0.7487699986    Its     17     Time Per It    0.0440452940
 Wall clock     3.012033939361572     
 Average time per cell    4.7063030302524566E-008
 Step time per cell       4.6819925308227538E-008
 Step       5 time   0.0160000 timestep   4.00E-03
Conduction error  0.3634224E-14
Iteration count       17
      Solve Time    0.7347021103    Its     17     Time Per It    0.0432177712
 Wall clock     3.748637914657593     
 Average time per cell    4.6857973933219909E-008
 Step time per cell       4.5939803123474123E-008
 Step       6 time   0.0200000 timestep   4.00E-03
Conduction error  0.3921679E-14
Iteration count       17
      Solve Time    0.7275660038    Its     17     Time Per It    0.0427980002
 Wall clock     4.478137016296387     
 Average time per cell    4.6647260586420693E-008
 Step time per cell       4.5494124293327334E-008
 Step       7 time   0.0240000 timestep   4.00E-03
Conduction error  0.4156725E-14
Iteration count       17
      Solve Time    0.8918459415    Its     17     Time Per It    0.0524615260
 Wall clock     5.371938943862915     
 Average time per cell    4.7963740570204596E-008
 Step time per cell       5.5762052536010744E-008
 Step       8 time   0.0280000 timestep   4.00E-03
Conduction error  0.4364243E-14
Iteration count       17
      Solve Time    0.7249619961    Its     17     Time Per It    0.0426448233
 Wall clock     6.098536968231201     
 Average time per cell    4.7644820064306261E-008
 Step time per cell       4.5314624905586242E-008
 Step       9 time   0.0320000 timestep   4.00E-03
Conduction error  0.4553778E-14
Iteration count       17
      Solve Time    0.7277460098    Its     17     Time Per It    0.0428085888
 Wall clock     6.826689004898071     
 Average time per cell    4.7407562534014386E-008
 Step time per cell       4.5506194233894351E-008
 Step      10 time   0.0360000 timestep   4.00E-03
Conduction error  0.2794031E-13
Iteration count       16
      Solve Time    0.7106130123    Its     16     Time Per It    0.0444133133
 Wall clock     7.539512872695923     
 Average time per cell    4.7121955454349517E-008
 Step time per cell       4.4453680515289310E-008
Test problem   5 is within   0.1059985E-08% of the expected solution
 This test is considered PASSED
 First step overhead   6.1355829238891602E-002
 Wall clock     7.541749954223633     
 
Profiler Output                 Time            Percentage
Timestep              :          0.0221          0.2930
Halo Exchange         :          0.0025          0.0336
Summary               :          0.0034          0.0451
Visit                 :          0.0000          0.0000
Tea Init              :          0.0230          0.3052
Dot Product           :          0.0082          0.1084
Tea Solve             :          7.4896         99.3080
Tea Reset             :          0.0015          0.0201
Set Field             :          0.0000          0.0006
Total                 :          7.5504        100.1141
The Rest              :         -0.0086         -0.1141
 
   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    512
 Thread Count:     1
 
 
   Tea Version    1.000
       MPI Version
    OpenMP Version
   Task Count    512
 Thread Count:     1
 
 Output file tea.out opened. All output will go there.
 Step       1 time   0.0000000 timestep   4.00E-03
Conduction error  0.6379851E-18
Iteration count       17
      Solve Time    0.7916119099    Its     17     Time Per It    0.0465654065
 Wall clock    0.7932679653167725     
 Average time per cell    4.9579247832298279E-008
 Step time per cell       4.9577116966247559E-008
 Step       2 time   0.0040000 timestep   4.00E-03
Conduction error  0.1066305E-14
Iteration count       17
      Solve Time    0.8612928391    Its     17     Time Per It    0.0506642847
 Wall clock     1.656476020812988     
 Average time per cell    5.1764875650405886E-008
 Step time per cell       5.3852498531341556E-008
 Step       3 time   0.0080000 timestep   4.00E-03
Conduction error  0.2539188E-14
Iteration count       17
      Solve Time    0.7343080044    Its     17     Time Per It    0.0431945885
 Wall clock     2.392714023590088     
 Average time per cell    4.9848208824793499E-008
 Step time per cell       4.5917376875877378E-008
 Step       4 time   0.0120000 timestep   4.00E-03
Conduction error  0.3227957E-14
Iteration count       17
      Solve Time    0.7359850407    Its     17     Time Per It    0.0432932377
 Wall clock     3.130598068237305     
 Average time per cell    4.8915594816207885E-008
 Step time per cell       4.6020492911338804E-008
 Step       5 time   0.0160000 timestep   4.00E-03
Conduction error  0.3634224E-14
Iteration count       17
      Solve Time    0.7317018509    Its     17     Time Per It    0.0430412853
 Wall clock     3.864199161529541     
 Average time per cell    4.8302489519119261E-008
 Step time per cell       4.5752570033073429E-008
 Step       6 time   0.0200000 timestep   4.00E-03
Conduction error  0.3921679E-14
Iteration count       17
      Solve Time    0.7267060280    Its     17     Time Per It    0.0427474134
 Wall clock     4.592813014984131     
 Average time per cell    4.7841802239418027E-008
 Step time per cell       4.5440241694450380E-008
 Step       7 time   0.0240000 timestep   4.00E-03
Conduction error  0.4156725E-14
Iteration count       17
      Solve Time    0.7341790199    Its     17     Time Per It    0.0431870012
 Wall clock     5.328624963760376     
 Average time per cell    4.7577008605003360E-008
 Step time per cell       4.5890554785728455E-008
 Step       8 time   0.0280000 timestep   4.00E-03
Conduction error  0.4364243E-14
Iteration count       17
      Solve Time    0.8552019596    Its     17     Time Per It    0.0503059976
 Wall clock     6.184220075607300     
 Average time per cell    4.8314219340682030E-008
 Step time per cell       5.3471371531486511E-008
 Step       9 time   0.0320000 timestep   4.00E-03
Conduction error  0.4553778E-14
Iteration count       17
      Solve Time    0.7230110168    Its     17     Time Per It    0.0425300598
 Wall clock     6.909152984619141     
 Average time per cell    4.7980229059855140E-008
 Step time per cell       4.5210242271423340E-008
 Step      10 time   0.0360000 timestep   4.00E-03
Conduction error  0.2794031E-13
Iteration count       16
      Solve Time    0.7080049515    Its     16     Time Per It    0.0442503095
 Wall clock     7.619379997253418     
 Average time per cell    4.7621124982833862E-008
 Step time per cell     Test problem   5 is within   0.1060041E-08% of the expected solution
  4.4288188219070434E-008
 This test is considered PASSED
 First step overhead  -6.8406105041503906E-002
 Wall clock     7.621680021286011     
 
Profiler Output                 Time            Percentage
Timestep              :          0.0226          0.2961
Halo Exchange         :          0.0023          0.0305
Summary               :          0.0034          0.0450
Visit                 :          0.0000          0.0000
Tea Init              :          0.0165          0.2159
Dot Product           :          0.0016          0.0207
Tea Solve             :          7.5762         99.4035
Tea Reset             :          0.0015          0.0192
Set Field             :          0.0000          0.0004
Total                 :          7.6241        100.0314
The Rest              :         -0.0024         -0.0314
